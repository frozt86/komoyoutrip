<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cabin extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'boat_id',
        'name',
        'type',
        'notes',
        'is_available',
    ];

    protected $casts = [
        'is_available' => 'boolean',
    ];

    public function boat()
    {
        return $this->belongsTo(Boat::class);
    }

    public function tourItems()
    {
        return $this->hasMany(TourItem::class);
    }
}
