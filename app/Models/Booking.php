<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'agent_id',
        'code',
        'client_name',
        'client_email',
        'client_mobile',
        'nominal',
        'midtrans_code',
        'payment_url',
        'canceled_at',
    ];

    protected $casts = [
        'canceled_at' => 'datetime',
    ];

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public function bookingItems()
    {
        return $this->hasMany(BookingItem::class);
    }

    public function guests()
    {
        return $this->hasMany(BookingGuest::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
