<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Boat extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'code',
        'name',
        'content',
        'notes',
        'is_maintained',
        'is_alive',
    ];

    protected $casts = [
        'is_maintained' => 'boolean',
        'is_alive' => 'boolean',
    ];

    public function cabins()
    {
        return $this->hasMany(Cabin::class);
    }

    public function schedules()
    {
        return $this->hasMany(BoatSchedule::class);
    }

    public function tours()
    {
        return $this->hasMany(Tour::class);
    }
}
