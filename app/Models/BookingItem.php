<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingItem extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'booking_id',
        'bookingitemable_id',
        'bookingitemable_type',
        'qty',
    ];

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    public function bookingitemable()
    {
        return $this->morphTo();
    }
}
