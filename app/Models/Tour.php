<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tour extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'boat_id',
        'code',
        'name',
        'tour_date_start',
        'tour_date_end',
        'content',
        'notes',
        'is_private',
        'published',
    ];

    protected $casts = [
        'tour_date_start' => 'datetime',
        'tour_date_end' => 'datetime',
        'is_private' => 'boolean',
        'published' => 'boolean',
    ];

    public function boat()
    {
        return $this->belongsTo(Boat::class);
    }

    public function tourItems()
    {
        return $this->hasMany(TourItem::class);
    }
}
