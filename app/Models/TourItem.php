<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TourItem extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'tour_id',
        'cabin_id',
        'name',
        'code',
        'price',
        'discount',
        'cashback',
        'maximum_book',
        'published',
    ];

    protected $casts = [
        'discount' => 'decimal:2',
        'cashback' => 'decimal:2',
        'published' => 'boolean',
    ];

    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }

    public function cabin()
    {
        return $this->belongsTo(Cabin::class);
    }
}
