<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BoatSchedule extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'boat_id',
        'schedule_title',
        'content',
        'date_start',
        'date_end',
        'is_maintained',
        'notes',
    ];

    protected $casts = [
        'date_start' => 'datetime',
        'date_end' => 'datetime',
        'is_maintained' => 'boolean',
    ];

    public function boat()
    {
        return $this->belongsTo(Boat::class);
    }
}
