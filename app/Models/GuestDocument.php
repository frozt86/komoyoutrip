<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GuestDocument extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'booking_id',
        'document_type',
        'url',
    ];

    protected $casts = [
        'document_type' => 'integer',
    ];

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }
}
