<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        # Tabel Admin
        Schema::create("admin", function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained("users");
            $table->string("name", 255)->unique()->index();
            $table->boolean("is_superadmin");
            $table->timestamps();
            $table->softDeletes();
        });

        # Tabel Agent
        Schema::create("agent", function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained("users");
            $table->string("code", 16)->unique()->index();
            $table->string("name", 255)->unique()->index();
            $table->string("mobile_number", 31)->unique()->index();
            $table->string("email", 255)->nullable()->unique()->index();
            $table->timestamps();
            $table->softDeletes();
        });

        # Tabel Boat
        Schema::create("boat", function (Blueprint $table) {
            $table->id();
            $table->string("code", 16)->unique()->index();
            $table->string("name", 255);
            $table->text("content");
            $table->text("notes")->nullable();
            $table->boolean("is_maintained");
            $table->boolean("is_alive");
            $table->timestamps();
            $table->softDeletes();
        });

        # Tabel Cabin
        Schema::create("cabin", function (Blueprint $table) {
            $table->id();
            $table->foreignId("boat_id")->constrained("boat");
            $table->string("name", 255);
            $table->string("type", 255);
            $table->text("notes")->nullable();
            $table->boolean("is_available");
            $table->timestamps();
            $table->softDeletes();
        });

        # Tabel Boat Schedule
        Schema::create("boat_schedule", function (Blueprint $table) {
            $table->id();
            $table->foreignId("boat_id")->constrained("boat");
            $table->string("schedule title", 255);
            $table->text("content");
            $table->timestamp("date_start");
            $table->timestamp("date_end");
            $table->boolean("is_maintaining");
            $table->text("notes")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        # Tabel Tour
        Schema::create("tour", function (Blueprint $table) {
            $table->id();
            $table->foreignId("boat_id")->constrained("boat");
            $table->string("code", 16)->unique()->index();
            $table->string("name", 255);
            $table->timestamp("tour_date_start");
            $table->timestamp("tour_date_end");
            $table->text("content");
            $table->text("notes")->nullable();
            $table->boolean("is_private");
            $table->boolean("published");
            $table->timestamps();
            $table->softDeletes();
        });

        # Tabel Tour Item
        Schema::create("tour_item", function (Blueprint $table) {
            $table->id();
            $table->foreignId("tour_id")->constrained("tour");
            $table->foreignId("cabin_id")->constrained("cabin");
            $table->string("name", 255);
            $table->string("code", 16)->unique()->index();
            $table->unsignedInteger("price");
            $table->decimal("discount");
            $table->decimal("cashback");
            $table->unsignedInteger("minimum_book");
            $table->unsignedInteger("maximum_book");
            $table->boolean("published");
            $table->timestamps();
            $table->softDeletes();
        });

        # Tabel Booking
        Schema::create("booking", function (Blueprint $table) {
            $table->id();
            $table->foreignId("agent_id")->nullable()->constrained("agent");
            $table->string("code", 16)->unique()->index();
            $table->string("client_name", 255);
            $table->string("client_email", 255);
            $table->string("client_mobile", 32);
            $table->unsignedInteger("nominal");
            $table->string("midtrans_code", 1023)->nullable();
            $table->text("payment_url")->nullable();
            $table->timestamp("canceled_at")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        # Tabel Booking Item
        Schema::create("booking_item", function (Blueprint $table) {
            $table->id();
            $table->foreignId("booking_id")->constrained("booking");
            $table->morphs("bookingitemable");
            $table->unsignedInteger("qty")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        # Tabel Booking Guest
        Schema::create("booking_guest", function (Blueprint $table) {
            $table->id();
            $table->foreignId("booking_id")->constrained("booking");
            $table->string("title", 16);
            $table->string("fullname", 255);
            $table->string("nationality", 127);
            $table->unsignedInteger("age");
            $table->text("notes")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        # Tabel guest document
        Schema::create("guest_document", function (Blueprint $table) {
            $table->id();
            $table->foreignId("booking_id")->constrained("booking");
            $table->unsignedTinyInteger("document_type")->comment("1: Image, 2: Pdf");
            $table->text("url");
            $table->timestamps();
            $table->softDeletes();
        });

        # Tabel Payment
        Schema::create("payment", function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->nullable()->constrained("users");
            $table->foreignId("booking_id")->constrained("booking");
            $table->string("code", 16)->unique()->index();
            $table->timestamp("payment_date");
            $table->unsignedTinyInteger("payment_type")->comment("1: Office Offline, 2: Agent Offline, 3: Online");
            $table->unsignedInteger("payment_nominal");
            $table->text("notes")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        # Tabel media
        Schema::create("media", function (Blueprint $table) {
            $table->id();
            $table->nullableMorphs("mediable");
            $table->unsignedTinyInteger("media_type")->comment("1: Image, 2: Video");
            $table->text("url");
            $table->boolean("thumbnail");
            $table->timestamps();
            $table->softDeletes();
        });

        # modify "Users" table
        Schema::table("users", function (Blueprint $table) {
            $table->unsignedInteger("telegram_id")->nullable()->unique()->index();
            $table->string("telegram_username", 255)->nullable()->unique()->index();
            $table->boolean("is_systemuser");

            # modify coloumn
            $table->string("email", 255)->nullable()->change();
            $table->string("password", 255)->nullable()->change();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::disableForeignKeyConstraints();

        Schema::table("users", function (Blueprint $table) {
            $table->dropColumn(["is_systemuser", "telegram_username", "telegram_id"]);
            $table->string("email")->change();
            $table->string("password")->change();
        });

        Schema::dropIfExists("media");
        Schema::dropIfExists("payment");
        Schema::dropIfExists("guest_document");
        Schema::dropIfExists("booking_guest");
        Schema::dropIfExists("booking_item");
        Schema::dropIfExists("booking");
        Schema::dropIfExists("tour_item");
        Schema::dropIfExists("tour");
        Schema::dropIfExists("boat_schedule");
        Schema::dropIfExists("cabin");
        Schema::dropIfExists("boat");
        Schema::dropIfExists("agent");
        Schema::dropIfExists("admin");
    }
};
